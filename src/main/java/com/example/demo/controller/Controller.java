package com.example.demo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class Controller {
	
	private Class<Controller> log=Controller.class;
	
	@GetMapping("getAllStudents")
	public Student[] getAllStudents(){
		RestTemplate restTemplate=new RestTemplate();
		Student[] student=null;
		
		try {
		 student=restTemplate.getForObject("https://springboot-database-excellent-parrot-tz.cfapps.io/getall", Student[].class);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		return student;
		
		
	}

}
